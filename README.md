# Automatic Face Shield

A helmet that uses a PIR sensor to detect if someone is moving nearby. The sensor will have a range of 3 to 5 meters and a 100 degree coverage. If someone is moving in range then the helmet will lower a face shield to cover your face. It will automaticly raise if it gets too hot and if it is dark it will light up. You can manualy lower and raise the shield by clicking or making a sound. Flip the swich to disable the microphone.
